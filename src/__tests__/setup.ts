import {initialize, LatLng, MVCObject, Size} from '@googlemaps/jest-mocks';
import '@testing-library/jest-dom/extend-expect';

jest.mock('../config/maps');

jest.spyOn(global.console, 'log').mockImplementation(() => jest.fn());
jest.spyOn(global.console, 'info').mockImplementation(() => jest.fn());
jest.spyOn(global.console, 'warn').mockImplementation(() => jest.fn());

initialize();

const MapsEventListener: google.maps.MapsEventListener = {
  remove: jest.fn(),
};

// Enhance the default mock with an actual method
global.google.maps.MVCObject.prototype.addListener = jest
  .fn()
  .mockReturnValue(MapsEventListener);

global.google.maps.event = {
  addListenerOnce: jest.fn().mockReturnValue(MapsEventListener),
  addListener: jest.fn().mockReturnValue(MapsEventListener),
  addDomListenerOnce: jest.fn().mockReturnValue(MapsEventListener),
  addDomListener: jest.fn().mockReturnValue(MapsEventListener),
  hasListeners: jest.fn(),
  clearInstanceListeners: jest.fn(),
  clearListeners: jest.fn(),
  removeListener: jest.fn(),
  trigger: jest.fn(),
};

global.google.maps.StreetViewService = class StreetViewService
  implements google.maps.StreetViewService
{
  async getPanorama() {
    const res: google.maps.StreetViewResponse = {
      data: {
        copyright: 'Copyright',
        imageDate: 'August 2020',
        links: [],
        location: {
          description: 'Fake panorama description',
          pano: '69',
          shortDescription: null,
          latLng: new LatLng(1, 2),
        },
        tiles: {
          centerHeading: 0,
          getTileUrl: () => '',
          tileSize: new Size(1, 1),
          worldSize: new Size(1, 1),
        },
      },
    };
    return Promise.resolve(res);
  }
};

global.google.maps.StreetViewPreference = {
  // @ts-expect-error - provide value for enum at runtime
  BEST: 'best',
  // @ts-expect-error - provide value for enum at runtime
  NEAREST: 'nearest',
};
global.google.maps.StreetViewSource = {
  // @ts-expect-error - provide value for enum at runtime
  OUTDOOR: 'outdoor',
  // @ts-expect-error - provide value for enum at runtime
  DEFAULT: 'default',
};

// TODO
global.google.maps.StreetViewPanorama = class MockStreetViewPanorama
  extends MVCObject
  implements google.maps.StreetViewPanorama
{
  controls = [];
  getLinks = jest.fn();
  registerPanoProvider = jest.fn();
  setLinks = jest.fn();
  getLocation = jest.fn();
  getMotionTracking = jest.fn();
  getPano = jest.fn();
  getPhotographerPov = jest.fn();
  getPosition = jest.fn();
  getPov = jest.fn();
  getStatus = jest.fn();
  getVisible = jest.fn();
  getZoom = jest.fn();
  setVisible = jest.fn();
  setPov = jest.fn();
  setPosition = jest.fn();
  setZoom = jest.fn();
  setLinksControl = jest.fn();
  setPanControl = jest.fn();
  setZoomControl = jest.fn();
  setMotionTracking = jest.fn();
  setMotionTrackingControl = jest.fn();
  setOptions = jest.fn();
  setPano = jest.fn();
  setPanControlOptions = jest.fn();
  setMotionTrackingOptions = jest.fn();
  setMotionTrackingControlOptions = jest.fn();
  setZoomControlOptions = jest.fn();
};
